resource "aws_instance" "testinst" {
  ami           = "ami-03653b248ab5aa41a"
  instance_type = "t2.micro"
  key_name      = "Ohio_key"

  # provisioner "local-exec" {
  #     command = "echo ${self.private_ip} >> private_ips.txt"
  # }
  provisioner "file" {
    source      = "test.yml"
    destination = "/home/ec2-user/test.yml"
  }
  connection {
    user        = "ec2-user"
    type        = "ssh"
    private_key = file("Ohio_key.pem")
    host        = self.public_ip
  }
}
